extends Node


export var max_enemy_bullets = 2
export var max_player_bullets = 1
export var enemy_formation_size = Vector2(8, 4)
export var enemy_shoot_speed_range = [1, 5]
export var enemy_block_speed = 10
export var enemy_block_move_right = true
var enemies = []
var enemy_bullets = []
var player_bullets = []
var rng = RandomNumberGenerator.new()
var Bullet = preload("Bullet.tscn")
var PlayerClass = preload("Player.gd")
var Enemy = preload("Enemy.tscn")
var score = 0


func _ready():
	$GameOver.hide()
	$Score.text = str(score)
	rng.randomize()

	for i in enemy_formation_size.y:
		enemies.push_back([])
		for j in enemy_formation_size.x:
			var enemy = Enemy.instance()
			enemy.get_node("Timer").wait_time = rng.callv(
				"randf_range", enemy_shoot_speed_range)
			enemy.connect(
				"ready_to_fire", self, "_on_Enemy_ready_to_fire", [enemy])
			enemy.connect("hit", self, "_on_Enemy_hit", [enemy])
			add_child(enemy)
			# Need to restart the timer in order to use the new speed
			enemy.get_node("Timer").stop()
			enemy.get_node("Timer").start()
			enemy.position.x = j * enemy.sprite_size.x
			enemy.position.y = i * enemy.sprite_size.y
			enemies[-1].push_back(enemy)


func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	_move_enemy_block(delta)


func _move_enemy_block(delta):
	var x_offset = enemy_block_speed * delta
	if not enemy_block_move_right:
		x_offset *= -1

	for i in enemy_formation_size.y:
		pass


func _on_Player_fired():
	_fire_bullet($Player, max_player_bullets, player_bullets)


func _on_Enemy_ready_to_fire(enemy):
	_fire_bullet(enemy, max_enemy_bullets, enemy_bullets)


func _fire_bullet(fired_by, max_bullets, bullet_array):
	# Clean up any dead bullets
	# TODO: Make this check every bullet, instead of just bullets at the back
	while bullet_array.size() > 0 and not bullet_array.back():
		bullet_array.pop_back()
	# Only have up to max_bullets on-screen at once
	if bullet_array.size() >= max_bullets and bullet_array.back():
		return

	var bullet = Bullet.instance()
	if fired_by is PlayerClass:
		bullet.fired_by = bullet.FiredBy.FIRED_BY_PLAYER
	else:
		bullet.fired_by = bullet.FiredBy.FIRED_BY_ENEMY
	add_child(bullet)

	bullet.position.x = fired_by.position.x + fired_by.sprite_size.x / 2 \
											- bullet.sprite_size.x / 2
	bullet.position.y = fired_by.position.y
	if fired_by is PlayerClass:
		bullet.position.y -= bullet.sprite_size.y
	else:
		bullet.position.y += fired_by.sprite_size.y + bullet.sprite_size.y

	bullet_array.push_front(bullet)


func _on_Player_hit(bullet):
	$Player.hide()
	$Player/CollisionShape2D.set_deferred("disabled", true)
	bullet.queue_free()
	$GameOver.show()
	yield(get_tree().create_timer(1), "timeout")
	$GameOver.hide()
	$Player.show()
	$Player/CollisionShape2D.disabled = false


func _on_Enemy_hit(bullet, enemy):
	bullet.queue_free()
	_increment_score()


func _increment_score(increment_amount=1):
	score += increment_amount
	$Score.text = str(score)
