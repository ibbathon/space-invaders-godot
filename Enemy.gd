extends Area2D


signal ready_to_fire
signal hit(bullet)
var rng = RandomNumberGenerator.new()


export var speed = 100
# Width and height in pixels (not post-scaling size)
export (Vector2) var sprite_size = Vector2(14,14)


func _ready():
	sprite_size.x *= $AnimatedSprite.scale.x
	sprite_size.y *= $AnimatedSprite.scale.y
	$AnimatedSprite.play()


func _on_Timer_timeout():
	emit_signal("ready_to_fire")


func _on_Enemy_body_entered(body):
	queue_free()
	emit_signal("hit", body)
