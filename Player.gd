extends Area2D

signal fired
signal hit(bullet)


export var speed = 400
var screen_size
# Width and height in pixels (not post-scaling size)
export (Vector2) var sprite_size = Vector2(18,18)


func _ready():
	sprite_size.x *= $AnimatedSprite.scale.x
	sprite_size.y *= $AnimatedSprite.scale.y
	$AnimatedSprite.play()
	screen_size = get_viewport_rect().size


func _process(delta):
	# Movement
	var velocity = Vector2()
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x - sprite_size.x)

	if velocity.x < 0:
		$AnimatedSprite.animation = "left"
	elif velocity.x > 0:
		$AnimatedSprite.animation = "right"
	else:
		$AnimatedSprite.animation = "idle"

	# Shooting
	if Input.is_action_just_pressed("ui_up"):
		emit_signal("fired")


func _on_Player_body_entered(body):
	emit_signal("hit", body)
