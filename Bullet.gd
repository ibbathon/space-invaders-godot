extends RigidBody2D


const FiredBy = {
	FIRED_BY_PLAYER = -1,
	FIRED_BY_ENEMY = 1,
}


export var speed = 400
export var fired_by = FiredBy.FIRED_BY_PLAYER
# Width and height in pixels (not post-scaling size)
export (Vector2) var sprite_size = Vector2(3,7)


func _ready():
	sprite_size.x *= $AnimatedSprite.scale.x
	sprite_size.y *= $AnimatedSprite.scale.y
	$AnimatedSprite.play()

	match fired_by:
		FiredBy.FIRED_BY_ENEMY:
			linear_velocity = Vector2(0, speed)
			$AnimatedSprite.scale.y *= -1
			$CollisionShape2D.scale.y *= -1
			$VisibilityNotifier2D.scale.y *= -1
			set_collision_mask_bit(0, false)
			set_collision_mask_bit(1, true)
		FiredBy.FIRED_BY_PLAYER:
			linear_velocity = Vector2(0, -speed)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
